﻿using Assets.Script.GenericScripts;
using UnityEngine;

namespace Assets.Script.Managers
{
    // Player State
    public enum State
    {
        NormalState = 0,
        HeavyState = 1,
        GiantState = 2
    };
    /// <summary>
    /// Manager to use for any program constants.
    /// </summary>
    public class ConstantsManager : Singleton<ConstantsManager>
    {


        /// <summary>
        /// Set the Constant volume for one particle in litter
        /// Def : 0.01 => 1 centi-litre.
        /// </summary>
        public float _ConstParticuleVolume = 0.01f;

        /// <summary>
        /// Epsilon for comparisons
        /// </summary>
        public float _Tolerance = 0.01f;

        /// <summary>
        /// Ratio between States
        /// </summary>
        public float _Ratio = 1.2f;

        /// <summary>
        /// Value for Detections RayCast
        /// </summary>
        public float[] _Skins = { 0.005f, 0.4f, 0.5f };


        public float _SelectedSkin = 0.005f;


        //Player Handling default state
        [HideInInspector]
        public float _NormalStateGravity;
        [HideInInspector]
        public float _NormalStateSpeed;
        [HideInInspector] 
        public float _NormalStateAcceleration;
        [HideInInspector] 
        public float _NormalStateMertrics;


        /// <summary>
        /// 
        /// </summary>
        public float _Gravity = 20.0f;
        /// <summary>
        /// 
        /// </summary>
        public float _Speed = 8.0f;
        /// <summary>
        /// 
        /// </summary>
        public float _Acceleration = 30.0f;
        /// <summary>
        /// 
        /// </summary>
        public float _Metrics = 12.0f;


        public State _PlayerState;

        /// <summary>
        /// Modifié par quentin.
        /// POur limiter le noimbre d'objets dans l'inspector
        /// </summary>
        public void Start()
        {
            _NormalStateGravity = _Gravity;
            _NormalStateSpeed = _Speed;
            _NormalStateAcceleration = _Acceleration;
            _NormalStateMertrics = _Metrics;
        }
    }
}
