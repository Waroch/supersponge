﻿
// use : DebugManager.isLogActivated

using Assets.Script.GenericScripts;

namespace Assets.Script.Managers
{
    /// <summary>
    /// Standart class for debug context.
    /// Use a Singleton design pattern
    /// </summary>
    public class DebugManager : Singleton<DebugManager>
    {
        // To desactivate on realese.
        public bool _IsLogActivated;

        // return true if log are enable
        /// <summary>
        /// Return the true if logging is allowed.
        /// False if they are not needed
        /// </summary>
        /// <returns>The validity of Log</returns>
        public bool IsLogActivated()
        {
            return _IsLogActivated;
        }
    }
}
