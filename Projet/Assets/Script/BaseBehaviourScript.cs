﻿using Assets.Script.Managers;
using UnityEngine;

namespace Assets.Script
{
    /// <summary>
    /// Base class to use to any component
    /// This will allow us to add easily extantions
    /// </summary>
    public class BaseBehaviourScript : MonoBehaviour
    {
        /// <summary>
        /// Try to print a message on the console.
        /// Check if lg are enable first.
        /// </summary>
        /// <param name="aLocalisationError">The class printing the error</param>
        /// <param name="aErrorMessage">The error message</param>
        public void DebugLog(string aLocalisationError, string aErrorMessage)
        {
            if (DebugManager.Instance.IsLogActivated())
            {
                Debug.Log(aLocalisationError + ", " + aErrorMessage);
            }
        }
    }
}
