﻿using System.Collections;
using System.IO;
using Assets.Script.GenericScripts;
using UnityEngine;

namespace Assets.Script
{
    public class TakeScreenshot : Singleton<TakeScreenshot>
    {

        public void TakeAScreenShot()
        {
            StartCoroutine(ScreenshotEncode());
        }

        public IEnumerator ScreenshotEncode()
        {
            // wait for graphics to render
            yield return new WaitForEndOfFrame();

            // create a texture to pass to encoding
            var texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

            // put buffer into texture
            texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            texture.Apply();

            // split the process up--ReadPixels() and the GetPixels() call inside of the encoder are both pretty heavy
            yield return 0;

            var bytes = texture.EncodeToPNG();

            // save our test image (could also upload to WWW)
            File.WriteAllBytes(Application.dataPath + "/../testscreen_" + System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".png", bytes);
            //_count++;

            // Added by Karl. - Tell unity to delete the texture, by default it seems to keep hold of it and memory crashes will occur after too many screenshots.
            DestroyObject(texture);
        }
    }
}