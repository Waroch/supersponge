using System;
using Assets.Script.Managers;
using UnityEngine;

namespace Assets.Script.ControllerScripts
{
    [RequireComponent(typeof(PlayerPhysics))]
    public class PlayerController : BaseBehaviourScript
    {

        private float _currentSpeed;
        private float _targetSpeed;
        private Vector2 _updateMove;

        private PlayerPhysics _playerPhysics;
        private PlayerState _playerState;

        //Transform
        private Vector3 _playerScale;
        private float _playerColiiderRadius;

        //Camera 
        private GameCamera _cam;


        public void Start()
        {
            _playerPhysics = GetComponent<PlayerPhysics>();
            _playerState = GetComponent<PlayerState>();
            _playerScale = new Vector3(transform.localScale.x, transform.localScale.y,
                transform.localScale.z);
            _playerColiiderRadius = GetComponent<CircleCollider2D>().radius;

            _currentSpeed = 0.0f;
            _targetSpeed = 0.0f;
            _updateMove = new Vector2(0.0f, 0.0f);

            _cam = Camera.main.GetComponent<GameCamera>();
        }

        public void Update()
        {
            // Reset acceleration upon collision
            if (_playerPhysics._MovementStopped)
            {
                _targetSpeed = 0;
                _currentSpeed = 0;
            }

            // If player is touching the ground
            if (_playerPhysics._Grounded)
            {
                _updateMove.y = 0;

                // Jump
                if (Input.GetButtonDown("Jump"))
                {
                    _updateMove.y = ConstantsManager.Instance._Metrics;
                }

                // ChangeState
                //if (Input.GetButtonDown(""))
                if (Input.GetKeyDown(KeyCode.S))
                {
                    switch (ConstantsManager.Instance._PlayerState)
                    {
                        case State.NormalState:
                            ConstantsManager.Instance._PlayerState = State.HeavyState;
                            ReorganizeScale(0.25f, 0.25f, 0.0f);
                            GetComponent<CircleCollider2D>().radius += 0.02f;
                            _playerPhysics.ColliderUpdate();
                            ReorganizePosition(transform.localScale.y / 2);
                            break;
                        case State.HeavyState:
                            ConstantsManager.Instance._PlayerState = State.GiantState;
                            ReorganizeScale(0.25f, 0.25f, 0.0f);
                            GetComponent<CircleCollider2D>().radius += 0.005f;
                            _playerPhysics.ColliderUpdate();
                            ReorganizePosition(transform.localScale.y / 2);
                            break;
                        case State.GiantState:
                            ConstantsManager.Instance._PlayerState = State.NormalState;
                            transform.localScale = _playerScale;
                            GetComponent<CircleCollider2D>().radius = _playerColiiderRadius;
                            _playerPhysics.ColliderUpdate();
                            ReorganizePosition(transform.localScale.y / 2);
                            break;
                    }
                    _playerState.UpdateMetrics();
                }
            }

            // Input
            _targetSpeed = Input.GetAxisRaw("Horizontal") * ConstantsManager.Instance._Speed;
            _currentSpeed = IncrementTowards(_currentSpeed, _targetSpeed, ConstantsManager.Instance._Acceleration);

            if (Math.Sign(_targetSpeed) < 0)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }
            if (Math.Sign(_targetSpeed) > 0)
            {
                 transform.eulerAngles = new Vector3(0, 0, 0);
            }

            // Set amount to move
            _updateMove.x = _currentSpeed;
            _updateMove.y -= ConstantsManager.Instance._Gravity * Time.deltaTime;
            _playerPhysics.Move(_updateMove * Time.deltaTime);

            //Set Cam
            _cam.SetTrackSpeed(_updateMove.x / ConstantsManager.Instance._Speed);
            CamTargetPosition();
        }

        //Reorganize player Position according to states
        private void ReorganizePosition(float s)
        {
            transform.Translate(new Vector3(0, s, 0));
        }

        //Reorganize player Scale according to states
        private void ReorganizeScale(float xRescale, float yRescale, float zRescale)
        {
            var xScale = transform.localScale.x;
            var yScale = transform.localScale.y;
            var zScale = transform.localScale.z;

            transform.localScale = new Vector3(xScale + xRescale, yScale + yRescale, zScale + zRescale);
        }

        //set the position of CamTarget
        private void CamTargetPosition()
        {
            if (!(Mathf.Abs(_updateMove.x) > 0.1f)) return;
            switch (ConstantsManager.Instance._PlayerState)
            {
                case State.NormalState:
                    transform.GetChild(0).transform.localPosition = new Vector3(5.0f, 0, 0);
                    break;
                case State.HeavyState:
                    transform.GetChild(0).transform.localPosition = new Vector3(4.0f, 0, 0);
                    break;
                case State.GiantState:
                    transform.GetChild(0).transform.localPosition = new Vector3(3.5f, 0, 0);
                    break;
            }
        }

        // Increase n towards target by speed
        private static float IncrementTowards(float n, float target, float a)
        {
            if (Math.Abs(n - target) < ConstantsManager.Instance._Tolerance)
                return n;
            else
            {
                var dir = Mathf.Sign(target - n); // must n be increased or decreased to get closer to target
                n += a * Time.deltaTime * dir;
                return (Math.Abs(dir - Mathf.Sign(target - n)) < ConstantsManager.Instance._Tolerance) ? n : target;
                // if n has now passed target then return target, otherwise return n
            }
        }
    }
}
