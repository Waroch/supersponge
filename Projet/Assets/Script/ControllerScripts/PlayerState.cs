﻿using Assets.Script.Managers;

namespace Assets.Script.ControllerScripts
{
    public class PlayerState : BaseBehaviourScript
    {
        
        public void Start () 
        {
            ConstantsManager.Instance._PlayerState = State.NormalState;
        }
        
        /// <summary>
        /// Change the player handling depending on the status
        /// </summary>
        public void UpdateMetrics()
        {
            var ratio = ConstantsManager.Instance._Ratio;
            switch (ConstantsManager.Instance._PlayerState)
            {
                case State.NormalState:
                    ConstantsManager.Instance._Gravity = ConstantsManager.Instance._NormalStateGravity;
                    ConstantsManager.Instance._Speed = ConstantsManager.Instance._NormalStateSpeed;
                    ConstantsManager.Instance._Acceleration = ConstantsManager.Instance._NormalStateAcceleration;
                    ConstantsManager.Instance._Metrics = ConstantsManager.Instance._NormalStateMertrics;
                    ConstantsManager.Instance._SelectedSkin = ConstantsManager.Instance._Skins[0];
                    break;
                case State.HeavyState:
                    ConstantsManager.Instance._Gravity = ConstantsManager.Instance._Gravity * ratio;
                    ConstantsManager.Instance._Speed = ConstantsManager.Instance._Speed / ratio;
                    ConstantsManager.Instance._Acceleration = ConstantsManager.Instance._Acceleration / ratio;
                    ConstantsManager.Instance._Metrics = ConstantsManager.Instance._Metrics / ratio;
                    ConstantsManager.Instance._SelectedSkin = ConstantsManager.Instance._Skins[1];
                    break;
                case State.GiantState:
                    ConstantsManager.Instance._Gravity = ConstantsManager.Instance._Gravity * ratio;
                    ConstantsManager.Instance._Speed = ConstantsManager.Instance._Speed / ratio;
                    ConstantsManager.Instance._Acceleration = ConstantsManager.Instance._Acceleration / ratio;
                    ConstantsManager.Instance._Metrics = ConstantsManager.Instance._Metrics / ratio;
                    ConstantsManager.Instance._SelectedSkin = ConstantsManager.Instance._Skins[2];
                    break;
            }
        }
    }
}
