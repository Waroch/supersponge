using UnityEngine;

namespace Assets.Script.ControllerScripts
{
    public class GameManager : BaseBehaviourScript
    {
	
        public GameObject _Player;
        public GameCamera _Cam;
        public Vector3 _InitialPosition;
        public Vector3 _InitialRotation;
	
        public void Start () {
            _Cam = GetComponent<GameCamera>();
            SpawnPlayer();
        }
	
        // Spawn player
        private void SpawnPlayer()
        {
            var rotation = Quaternion.identity;
            rotation.eulerAngles = _InitialRotation;
            var o = Instantiate(_Player, _InitialPosition, rotation) as GameObject;
            if (o != null)
            {
                _Cam.SetTarget(o.transform);
                _Cam.transform.position = new Vector3(o.transform.position.x, _Cam.transform.position.y, _Cam.transform.position.z);
            }
                
        }
    }
}
