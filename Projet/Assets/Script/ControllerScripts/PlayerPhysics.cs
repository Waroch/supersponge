using Assets.Script.Managers;
using UnityEngine;

namespace Assets.Script.ControllerScripts
{
    [RequireComponent (typeof(CircleCollider2D))]
    public class PlayerPhysics : BaseBehaviourScript
    {

        public LayerMask _CollisionMask;

        private float _rayonCollider;
        private Vector2 _centerCollider;

        [HideInInspector]
        public bool _Grounded;
        [HideInInspector]
        public bool _MovementStopped;

	
        public void Start() {
            var circleCollider2D = GetComponent<CircleCollider2D>();
            _rayonCollider = circleCollider2D.radius;
            _centerCollider = circleCollider2D.center;
        }

        public void ColliderUpdate()
        {
            var circleCollider2D = GetComponent<CircleCollider2D>();
            _rayonCollider = circleCollider2D.radius;
            _centerCollider = circleCollider2D.center;
        }
        public void Move(Vector2 moveAmount) {
		
            var deltaY = moveAmount.y;
            var deltaX = moveAmount.x;
            Vector2 p = transform.position;

            // Check collisions above and below
            _Grounded = false;
            rigidbody2D.isKinematic = true;
            for (var i = 0; i < 3; i++)
            {
                var dir = Mathf.Sign(deltaY);
                var x = (p.x + _centerCollider.x - _rayonCollider/1.5f) + _rayonCollider/1.5f * i ; // centrepoint of collider
                var y = p.y + _centerCollider.y + _rayonCollider * dir; // Bottom of collider

                var ray = new Ray2D(new Vector2(x, y), new Vector2(0, dir));
                Debug.DrawRay(ray.origin, ray.direction, Color.black);

                if (Physics2D.Raycast(ray.origin, ray.direction, Mathf.Abs(deltaY) + ConstantsManager.Instance._SelectedSkin, _CollisionMask))
                {
                    rigidbody2D.isKinematic = false;
                    _Grounded = true;
                }
            }

            // Check collisions left and right
            _MovementStopped = false;
            for (var i = 0; i < 3; i++)
            {
                var dir = Mathf.Sign(deltaX);
                var x = p.x + _centerCollider.x + _rayonCollider * dir;
                var y = (p.y + _centerCollider.y - _rayonCollider / 1.5f) + _rayonCollider / 1.5f * i;

                var ray = new Ray2D(new Vector2(x, y), new Vector2(dir, 0));
                Debug.DrawRay(ray.origin, ray.direction, Color.red);

                var hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Abs(deltaX) + ConstantsManager.Instance._SelectedSkin, _CollisionMask);

                if (hit)
                {
                    // Get Distance between player and ground
                    var dst = Vector3.Distance(ray.origin, hit.point);

                    // Stop player's downwards movement after coming within skin width of a collider
                    if (dst > ConstantsManager.Instance._SelectedSkin)
                    {
                        deltaX = dst*dir - ConstantsManager.Instance._SelectedSkin*dir;
                    }
                    else
                    {
                        deltaX = 0;
                    }

                    _MovementStopped = true;
                    break;
                }
            }

            if (_Grounded)
            {
                deltaY = 0;
            }
            var finalTransform = new Vector2(deltaX, deltaY);
            transform.Translate(finalTransform,0);
        }
	
    }
}
