using System;
using Assets.Script.Managers;
using UnityEngine;

namespace Assets.Script.ControllerScripts
{
    public class GameCamera : BaseBehaviourScript
    {

        private Transform _target;
        public float _TrackSpeedX = 5.0f;
        public float _TrackSpeedY = 10.0f;

        private float _trackSpeedX;

        public float  _LimitX = 100.0f;
        public float  _LimitY = 100.0f;
     
        private float _minX;
        private float _maxX;
        private float _minY;
        private float _maxY;

        public void Start() 
        {
            var vertExtent = Camera.main.camera.orthographicSize;    
            var horzExtent = vertExtent * Screen.width / Screen.height;
 
            // Calculations assume map is position at the origin
            _minX = horzExtent - _LimitX / 2.0f;
            _maxX = _LimitX / 2.0f - horzExtent;
            _minY = vertExtent - _LimitY / 2.0f;
            _maxY = _LimitY / 2.0f - vertExtent;

            _trackSpeedX = _TrackSpeedX;
        }

        public void SetTrackSpeed(float t)
        {
            _trackSpeedX = Mathf.Abs(_TrackSpeedX * t);
        }

        // Set target
        public void SetTarget(Transform t) {
            _target = t;
        }
	
        // Track target
        public void LateUpdate() {
            if (!_target) return;
            var x = IncrementTowards(transform.position.x, _target.GetChild(0).position.x, _trackSpeedX);
            var y = IncrementTowards(transform.position.y, _target.GetChild(0).position.y, _TrackSpeedY);
            x = Mathf.Clamp(x, _minX, _maxX);
            y = Mathf.Clamp(y, _minY, _maxY);
            transform.position = new Vector3(x, y,transform.position.z);
        }

        // Increase n towards target by speed
        private static float IncrementTowards(float n, float target, float a)
        {
            if (Math.Abs(n - target) < ConstantsManager.Instance._Tolerance)
            {
                return n;
            }
            else
            {
                var dir = Mathf.Sign(target - n); // must n be increased or decreased to get closer to target
                n += a * Time.deltaTime * dir;
                return (Math.Abs(dir - Mathf.Sign(target - n)) < ConstantsManager.Instance._Tolerance) ? n : target; // if n has now passed target then return target, otherwise return n
            }
        }
    }
}
