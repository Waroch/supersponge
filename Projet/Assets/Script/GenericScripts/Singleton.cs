﻿
namespace Assets.Script.GenericScripts
{
    /// <summary>
    /// Generic singleton.
    /// To Use this design patern, create an new class and heritage like :
    /// MaClasse :  Singleton //MaClasse//
    /// </summary>
    /// <typeparam name="T">every thing</typeparam>
    public class Singleton<T> : BaseBehaviourScript where T : BaseBehaviourScript
    {
        /// <summary>
        /// Local instance
        /// </summary>
        private static T _instance;
        /// <summary>
        /// accesseur
        /// </summary>
        public static T Instance
        {
            get
            {
                if (_instance != null) return _instance;

                _instance = FindObjectOfType<T>();

                return _instance;
            }
        }

        /// <summary>
        /// Awake action
        /// </summary>
        public virtual void Awake()
        {
            DontDestroyOnLoad(gameObject);
            if (_instance == null)
            {
                _instance = this as T;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
