﻿using UnityEngine;

namespace Assets.Script.Liquid
{
    /// <summary>
    /// Continuous liquid.
    /// Use to provide too much particles on screen
    /// </summary>
    public class ContinuousLiquid : AbsorbtionLiquidScript
    {
        /// <summary>
        /// Scale factor for debug
        /// </summary>
        [Range(0.001f, 1f)]
        public float _ScaleFactor;

        /// <summary>
        /// Minimum Heigth for view
        /// </summary>
        public float _MinVolumeInMetrics;

        /// <summary>
        /// Maximum Heigth for view
        /// </summary>
        public float _MaxVolumeInMetrics;

        /// <summary>
        /// Locale base for improved resizing
        /// </summary>
        private float _yBase;

        // Use this for initialization
        public new void Start()
        {
            base.Start();
            _yBase = transform.position.y - transform.localScale.y;
            LiquidUpdate();
        }

        /// <summary>
        /// Custom update on collided
        /// </summary>
        public new void OnTriggerEnter2D(Collider2D other)
        //public new void OnCollisionEnter2D(Collision2D other)
        {
            base.OnTriggerEnter2D(other);
            //base.OnCollisionEnter2D(other);
            LiquidUpdate();
        }

        /// <summary>
        /// pdate view
        /// </summary>
        public void LiquidUpdate()
        {
            _ScaleFactor = VolumeStock / _MaxVolumeInLitter;
            ViewVolume();
        }

        /// <summary>
        /// Viewer for volume
        /// </summary>
        public void ViewVolume()
        {
            var correction = (_MaxVolumeInMetrics - _MinVolumeInMetrics) * _ScaleFactor + _MinVolumeInMetrics;
            transform.localScale = new Vector3(transform.localScale.x, correction, transform.localScale.z);
            transform.position = new Vector3(transform.position.x, _yBase + correction / 2, transform.position.z);
        }
    }
}
