﻿using UnityEngine;

namespace Assets.Script.Liquid
{
    /// <summary>
    /// Generic class for data managing
    /// </summary>
    public class GenericLauncherParticleScript : BaseBehaviourScript
    {
        /// <summary>
        /// Direction des particules
        /// </summary>
        public Vector2 _VectorDirectionParticule = Vector2.one;

        /// <summary>
        /// Force de lancement des particules
        /// </summary>
        public float _LaunchForce = 0.0f;

        /// <summary>
        /// Temps de vie des particules
        /// </summary>
        public float _ParticuleLifeTime = 3.0f;

        /// <summary>
        /// Type de particules a
        /// </summary>
        public ParticleScript.States _ParticleType;

        /// <summary>
        /// Optimisation if visible or not
        /// </summary>
        protected bool _CanSpawnObjects;

        public void Start()
        {
            _CanSpawnObjects = true;
        }

        /// <summary>
        /// Add a new particle
        /// </summary>
        public void SpawnObject()
        {
            if (_CanSpawnObjects)
            {
                // Spawn a particle
                var newLiquidParticle = ParticlePoolScript.Instance.GetObject();
                if (newLiquidParticle != null)
                {
                    newLiquidParticle.SetActive(true);
                    // stop!
                    newLiquidParticle.rigidbody2D.isKinematic = true;
                    // reset!
                    newLiquidParticle.rigidbody2D.isKinematic = false;
                    // Add our custom force
                    newLiquidParticle.rigidbody2D.AddForce(_VectorDirectionParticule * _LaunchForce);
                    // Get the particle script
                    var particleScript = newLiquidParticle.GetComponent<ParticleScript>();
                    // Set each particle lifetime
                    particleScript.SetLifeTime(_ParticuleLifeTime);
                    // Set the particle State
                    particleScript.SetState(_ParticleType);
                    // Relocate to the spawner position
                    newLiquidParticle.transform.position = transform.position + new Vector3(_VectorDirectionParticule.x, _VectorDirectionParticule.y);
                }
            }
        }
    }
}
