﻿using UnityEngine;

namespace Assets.Script.Liquid
{
    /// <summary>
    /// Class for gesture data between player and data:
    /// 1 Absorb water
    /// 2 launch watter
    /// </summary>
    [RequireComponent(typeof(ParticleShootScript))]
    public class PlayerLiquidControler : AbsorbtionLiquidScript
    {
        /// <summary>
        /// Interval between two shots in seccond
        /// </summary>
        public float _TimeIntervalBetween2ShotsInsecconds = 0.5f;

        /// <summary>
        /// Instance of Shoot script
        /// </summary>
        private ParticleShootScript _shootScript;

        /// <summary>
        /// Allow the player to shoot with time
        /// </summary>
        private bool _isPlayerAbleToShoot;


        // Use this for initialization
        public new void Start()
        {
            base.Start();
            _shootScript = GetComponent<ParticleShootScript>();
            Reset();
        }

        // Update is called once per frame
        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                TryToShoot();
            }
        }

        /// <summary>
        /// Shot if there is enought watter plus if _isPlayerAbleToShoot == true
        /// </summary>
        public void TryToShoot()
        {
            if (VolumeStock > _shootScript._VolumePerShoot && _isPlayerAbleToShoot)
            {
                VolumeStock -= _shootScript._VolumePerShoot;
                _shootScript.Shoot(_shootScript._VectorDirectionParticule, _shootScript._LaunchForce);
                _isPlayerAbleToShoot = false;
                Invoke("Reset", _TimeIntervalBetween2ShotsInsecconds);
            }
            // TODO Delegate sur une animation ?
        }

        /// <summary>
        /// Reset PlayerState
        /// </summary>
        private void Reset()
        {
            _isPlayerAbleToShoot = true;
        }

        /// <summary>
        /// TODO : Changer avec un degug draw
        /// </summary>
        public void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (_shootScript != null && _shootScript._VectorDirectionParticule != null)
            {
                Gizmos.DrawRay(transform.position, _shootScript._VectorDirectionParticule);
            }
        }
    }
}
