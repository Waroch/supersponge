﻿
using System.Collections;
using UnityEngine;

namespace Assets.Script.Liquid
{
    /// <summary>
    /// Class 
    /// </summary>
    public class ParticleShootScript : GenericLauncherParticleScript
    {
        /// <summary>
        /// Volume projeté en un tir
        /// </summary>
        public float _VolumePerShoot = 1f;

        /// <summary>
        /// Time interval between 2 particles
        /// </summary>
        public float _TimeIntervalBetwen2Shots = 0.001f;

        /// <summary>
        /// Nombre de particlules par litre
        /// </summary>
        private const int _nbParticlePerLitre = 10;
        
        /// <summary>
        /// Draw a single Shoot
        /// </summary>
        /// <param name="aDirection">Shoot direction</param>
        /// <param name="aForce">Shoot force</param>
        public void Shoot(Vector2 aDirection, float aForce)
        {
            StartCoroutine(ShootRoutine(aDirection, aForce));
        }

        /// <summary>
        /// Modify the direction
        /// </summary>
        /// <param name="aNewVector2">The new Vector</param>
        public void ModifyVectorShoot(Vector2 aNewVector2)
        {
            _VectorDirectionParticule = aNewVector2.normalized;
        }

        /// <summary>
        /// Shoot Routine
        /// </summary>
        /// <param name="aDirection">Shoot direction</param>
        /// <param name="aForce">Shoot force</param>
        /// <returns>The yield state</returns>
        private IEnumerator ShootRoutine(Vector2 aDirection, float aForce)
        {
            var max = _nbParticlePerLitre * _VolumePerShoot;
            _LaunchForce = aForce;
            _VectorDirectionParticule = aDirection;
            for (var i = 0; i < max; i++)
            {
                SpawnObject();
                yield return new WaitForSeconds(_TimeIntervalBetwen2Shots);
            }
            yield return null;
        }
    }
}
