﻿
using UnityEngine;

namespace Assets.Script.Liquid
{
    /// <summary>
    /// Cette classe permet de simuler le fonctionnnement d'une source
    /// </summary>
    public class ParticleGeneratorScript : GenericLauncherParticleScript
    {

        /// <summary>
        /// Intervale de temps entre deux spawns.
        /// En seccondes. => 0.1f => 0,1 secconde
        /// </summary>
        public float _ReapetingTimeInterval = 0.1f;

        /// <summary>
        /// Constant time to wait before animation
        /// </summary>
        private const float _timeToWait = 3f;


        public void Update()
        {
            _CanSpawnObjects = renderer.isVisible;
        }

        // Use this for initialization
        public void Awake()
        {
            // Wait a  fwe befoe loading
            InvokeRepeating("SpawnObject", _timeToWait, _ReapetingTimeInterval);
        }
    }
}
