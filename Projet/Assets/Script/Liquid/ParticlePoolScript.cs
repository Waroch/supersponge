﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using Assets.Script.GenericScripts;

namespace Assets.Script.Liquid
{
    /// <summary>
    /// pool manager pour les particules
    /// </summary>
    public class ParticlePoolScript : Singleton<ParticlePoolScript>
    {
        /// <summary>
        /// Nombre de particules à gêrer coté pool
        /// </summary>
        public int _NumberParticulesInCache;
        
        /// <summary>
        /// Relative path to prefeb.
        /// </summary>
        private string _pathName;

        /// <summary>
        /// Generic List
        /// </summary>
        private List<GameObject> _arrayParticles;


        /// <summary>
        /// The last index used.
        /// </summary>
        private int _index;

        /// <summary>
        /// Return an object in the list.
        /// </summary>
        /// <returns>_arrayParticles[_index]</returns>
        public GameObject GetObject()
        {
            _index++;
            if (_index >= _NumberParticulesInCache)
            {
                _index = 0;
            }
            return _arrayParticles[_index];
        }

        // Use this for initialization
        public void Start()
        {
            _pathName = "Particles/Particle";
            _arrayParticles = new List<GameObject>();
            _index = 0;

            StartCoroutine(SpawnParticle());
        }

        /// <summary>
        /// Spawn the _NumberParticulesInCache particle
        /// </summary>
        /// <returns>null</returns>
        private IEnumerator SpawnParticle()
        {
            for (var i = 0; i < _NumberParticulesInCache; i++)
            {
                var newLiquidParticle = (GameObject)Instantiate(Resources.Load(_pathName)); //Spawn a particle
                newLiquidParticle.SetActive(false);
                newLiquidParticle.transform.parent = transform;
                _arrayParticles.Add(newLiquidParticle);
            }
            yield return null;
        }
    }
}
