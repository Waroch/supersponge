﻿
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Liquid
{
    /// <summary>
    /// ScriptGerant les particules
    /// </summary>
    public class ParticleScript : BaseBehaviourScript
    {
        /// <summary>
        /// The 3 states of the particle
        /// </summary>
        public enum States { Water, Gas, Lava, None };

        /// <summary>
        /// How much time before the particle should live
        /// </summary>
        public float _ParticleLifeTime = 3.0f;

        /// <summary>
        /// Materials for watter, gas, lava.
        /// </summary>
        public List<GameObject> _Materials;

        /// <summary>
        /// Delegate pour rendre le code plus générique
        /// </summary>
        private Action _actionForParticle;

        /// <summary>
        /// How much time before the particle scalesdown and dies
        /// </summary>
        private volatile float _startTime;

        /// <summary>
        /// Particle State
        /// </summary>
        private States _currentState;

        /// <summary>
        /// Constant definition pour le flotement d'un gas
        /// How fast does the gas goes up?
        /// </summary>
        private const float _gasFloatability = 7.0f;

        /// <summary>
        /// Return the curent state
        /// </summary>
        /// <returns>_currentState</returns>
        public States CurentState()
        {
            return _currentState;
        }

        /// <summary>
        /// Renderer to resize
        /// </summary>
        public GameObject _Renderer;

        /// <summary>
        /// Basic initialisation.
        /// Initialise at watter state
        /// </summary>
        public void Start()
        {
            _currentState = States.Water;
            _actionForParticle = WatterAnimation;
            rigidbody2D.gravityScale = 1.0f; // To simulate Water density

            // Reset the particle velocity	
            rigidbody2D.velocity = Vector2.zero;
            // On selectionne la texture souhaitee
            _Materials[(int)_currentState].SetActive(true);
        }

        // The definitios to each state
        public void SetState(States newState)
        {
            // Only change to a different state
            if (newState != _currentState)
            {
                switch (newState)
                {
                    case States.Water:
                        _actionForParticle = WatterAnimation;
                        rigidbody2D.gravityScale = 1.0f; // To simulate Water density
                        break;
                    case States.Gas:
                        _actionForParticle = GasAnimation;
                        // Gas lives the time the other particles
                        _ParticleLifeTime = _ParticleLifeTime / 2.0f;
                        // To simulate Gas density
                        rigidbody2D.gravityScale = 0.0f;
                        // To have a different collision layer than the other particles 
                        // (so gas doesnt rises up the lava but still collides with the wolrd)
                        gameObject.layer = LayerMask.NameToLayer("Gas");
                        break;
                    case States.Lava:
                        // Same animation
                        _actionForParticle = WatterAnimation;
                        // To simulate the lava density
                        rigidbody2D.gravityScale = 0.3f;
                        break;
                    case States.None:
                        _actionForParticle = null;
                        break;
                }
                _Materials[(int)_currentState].SetActive(false);
                _currentState = newState;
                _Materials[(int)_currentState].SetActive(true);

                if (newState == States.None) return;

                // Reset the particle velocity	
                rigidbody2D.velocity = Vector2.zero;
                // On selectionne la texture souhaitee
                //_Renderer.GetComponent<MeshRenderer>().materials[0] = _Materials[(int)_currentState];
                // Reset the life of the particle on a state change
                _startTime = Time.time;
            }
        }

        /// <summary>
        /// Fixed update pour la physique.
        /// C'est beaucoup plus performant
        /// </summary>
        public void FixedUpdate()
        {
            if (_actionForParticle != null)
            {
                _actionForParticle();
            }
        }

        /// <summary>
        /// Do the correct water animation
        /// </summary>
        public void WatterAnimation()
        {
            ScaleDown();
        }

        /// <summary>
        /// Function for gas interaction.
        /// </summary>
        public void GasAnimation()
        {
            if (rigidbody2D.velocity.y < 50)
            {
                // Limits the speed in Y to avoid reaching mach 7 in speed
                // Gas always goes upwards
                rigidbody2D.AddForce(new Vector2(0, _gasFloatability));
            }
            ScaleDown();
        }


        // The effect for the particle to seem to fade away
        void ScaleDown()
        {
            var scaleValue = 1.0f - ((Time.time - _startTime) / _ParticleLifeTime);
            //DebugLog("ScaleDown", "scale " +scaleValue);
            if (scaleValue < 0.1f)
            {
                //_actionForParticle = null;
                gameObject.SetActive(false);
                // Destroy(gameObject);
            }
            else
            {
                // Solution la plus performante car on ne resize pas le collider.
                // Il faut le garder petit
                //_Renderer.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, scaleValue);
                _Renderer.transform.localScale = Vector3.one * scaleValue;
            }
        }

        // To change particles lifetime externally (like the particle generator)
        public void SetLifeTime(float time)
        {
            _startTime = Time.time;
            _ParticleLifeTime = time;
        }

        // Here we handle the collision events with another particles, in this example water+lava= water-> gas
        //public void OnTriggerEnter2D(Collider2D other)
        public void OnCollisionEnter2D(Collision2D other)
        {
            if (_currentState == States.Water && other.gameObject.tag == "Particle")
            {
                if (other.collider.GetComponent<ParticleScript>().CurentState() == States.Lava)
                {
                    SetState(States.Gas);
                }
            }
        }
    }


}
