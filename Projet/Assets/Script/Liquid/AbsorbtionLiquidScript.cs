﻿using Assets.Script.Managers;
using UnityEngine;

namespace Assets.Script.Liquid
{
    /// <summary>
    /// Class for liquid Absorbtion
    /// Store a volume.
    /// </summary>
    public class AbsorbtionLiquidScript : BaseBehaviourScript
    {
        /// <summary>
        /// Local volume in stock
        /// </summary>
        protected float VolumeStock { get; set; }

        /// <summary>
        /// Action To do When a collision is valiated
        /// </summary>
        public EventDelegate _OnCollidedFunction;

        /// <summary>
        /// Local boolean to set to fasle in case the object isfull
        /// </summary>
        protected bool _IsFull;

        /// <summary>
        /// Maximum Volume in litters
        /// </summary>
        public float _MaxVolumeInLitter;
        /// <summary>
        /// Base volume in litters
        /// </summary>
        public float _InitVolume;

        // Initialization
        public void Start()
        {
            VolumeStock = _InitVolume;
            _IsFull = false;
        }


        // Absorb The particle
        public void OnTriggerEnter2D(Collider2D other)
        //public void OnCollisionEnter2D(Collision2D other)
        {
            _IsFull = VolumeStock > _MaxVolumeInLitter;

            if (other.gameObject.tag == "Particle" && !_IsFull)
            {
                //DebugLog("OnCollisionEnter2D", "hit" + ConstantsManager.Instance._ConstParticuleVolume);
                VolumeStock += ConstantsManager.Instance._ConstParticuleVolume;

                // Desactive la goutte
                other.gameObject.SetActive(false);

                // Activate Delegate
                if (_OnCollidedFunction.isValid)
                {
                    _OnCollidedFunction.Execute();
                }

            }

            // TODO If absorbable
        }
    }
}
